<?php
/**
 * ClusterPress Doc Archive root template.
 *
 * @package ClusterPress Doc\templates\doc\archive
 * @subpackage index
 *
 * @since 1.0.0
 */

cp_feedbacks(); ?>

<?php cp_get_template_part( 'assets/filters' ) ; ?>

<div id="cp-doc-archive" class="doc archive">

	<?php if ( cp_doc_is_search() ) :

		cp_doc_add_site_posts_filters();

		cp_get_template_part( 'site/loops/posts' );

		cp_doc_remove_site_posts_filters();

	else :

		cp_get_template_part( 'doc/loops/toc' );

	endif ; ?>

</div>
