<?php
/**
 * Doc's Cluster.
 *
 * @package ClusterPress Doc\doc\classes
 * @subpackage doc-cluster
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * The Doc's Cluster Class.
 *
 * @since 1.0.0
 */
class CP_Doc_Cluster extends CP_Cluster {

	/**
	 * The constructor
	 *
	 * @since 1.0.0
	 *
	 * @param array args The Doc's Cluster arguments.
	 * {@see CP_Cluster::__construct() for a detailled list of available arguments}
	 */
	public function __construct( $args = array() )  {
		parent::__construct( $args );
	}

	/**
	 * Add the Doc's Cluster to ClusterPress main instance.
	 *
	 * @since 1.0.0
	 */
	public static function start() {
		$cp = clusterpress();

		if ( empty( $cp->doc ) ) {
			$cp->doc = new self( array(
				'id'            => 'doc',
				'name'          => __( 'Documentation de ClusterPress', 'clusterpress-doc' ),
				'absdir'        => plugin_dir_path( dirname( __FILE__ ) ),
				'slug'          => 'doc',
				'rewrite_id'    => 'cpd_documentation',
				'has_single'    => false,
			) );
		}

		return $cp->doc;
	}

	/**
	 * Set additional globals
	 *
	 * @since 1.0.0
	 */
	public function set_up_additional_globals() {
		$this->version = '1.0.0-beta2';

		$plugin_basename = dirname( dirname( __FILE__ ) );
		$this->path_data = array(
			'dir' => plugin_dir_path( $plugin_basename ),
			'url' => plugin_dir_url ( $plugin_basename ),
		);

		parent::set_up_additional_globals();
	}

	/**
	 * Include the needed files.
	 *
	 * @since  1.0.0
	 *
	 * @param  array  $files The list of files and class files
	 */
	public function load_cluster( $files = array() ) {
		// Only load files when on subsites
		if ( ! cp_is_main_site() ) {
			$files = array(
				'files'       => array(
					'functions',
					'tags',
					'filters',
				),
				'class_files' => array(
					'doc-screens',
					'doc-toc-items-loop',
					'doc-toc',
				),
			);

			if ( is_admin() ) {
				$files['files'][] = 'admin';
			}

			// Load the Loop and tags of the site Cluster if disabled.
			if ( ! cp_cluster_is_enabled( 'site' ) ) {
				$files['files'] = array_merge( $files['files'], array(
					cp_get_plugin_dir() . 'site/tags',
				) );

				$files['class_files'] = array_merge( $files['class_files'], array(
					cp_get_plugin_dir() .'site/site-posts-loop',
				) );
			}

		// Load specific files for the Site's Cluster documentation section.
		} elseif ( cp_cluster_is_enabled( 'site' ) && is_multisite() ) {
			$files = array(
				'files' => array(
					'functions',
					'filters',
					'site/tags',
					'site/actions',
				),
				'class_files' => array(
					'doc-toc-items-loop',
				)
			);
		}

		if ( cp_is_main_site() && is_admin() ) {
			$files['files'][] = 'settings';
		}

		parent::load_cluster( $files );
	}

	/**
	 * Set specific hooks for the Doc's Cluster.
	 *
	 * Avoid overriding parent methods so that this class's methods
	 * are not triggered when on the main site.
	 *
	 * @since 1.0.0
	 */
	public function set_cluster_hooks() {
		// Load languages
		add_action( 'cp_include_clusters', array( $this, 'load_languages' ) );

		// Doc will only be used in subsites of the network
		if ( ! cp_is_main_site() ) {

			// Set Doc globals according to the subsite query.
			add_action( 'cp_parse_query',         array( $this, 'parse_subsite_query' ), 11, 1 );

			// Register the post type on subsite
			add_action( 'cp_register_post_types', 'cp_doc_register_post_type' );

			// Register taxonomies on subsite
			add_action( 'cp_register_taxonomies', 'cp_doc_register_taxonomy'  );

			// Set up page title on subsite
			add_action( 'cp_setup_page_title',    array( $this, 'set_subsite_page_title' ), 10, 0 );

			// Highlight the nav menu item if it matches the documentation root url.
			add_filter( 'wp_get_nav_menu_items',  'cp_doc_get_nav_menu_items', 10, 1 );
		}

		if ( ! is_multisite() ) {
			add_action( 'cp_admin_notices', array( $this, 'admin_notice' ) );
		}
	}

	/**
	 * Displays a notice if the current config is not multisite.
	 *
	 * @since  1.0.0
	 *
	 * @return HTML Output.
	 */
	public function admin_notice() {
		$notices = array(
			esc_html__( 'ClusterPress Doc nécessite que votre configuration de WordPress soit multisite.', 'clusterpress-doc' ),
			esc_html__( 'Merci de configurer le multisite sur votre WordPress ou de désactiver ce Cluster.', 'clusterpress-doc' ),
		);

		printf( '<div class="error notice is-dismissible">%s</div>', '<p>' . join( '</p><p>', $notices ) . '</p>' );
	}

	/**
	 * Analyse the WordPress Query to eventually set ClusterPress Doc globals.
	 *
	 * @since 1.0.0
	 *
	 * @param WP_Query $q The WordPress main query.
	 */
	public function parse_subsite_query( $q = null ) {
		if ( ! cp_can_alter_posts_query( $q ) ) {
			return;
		}

		$cp           = clusterpress();
		$chapter_slug = $q->get( 'cp_chapter' );

		if ( 'cp_doc' === $q->get( 'post_type' ) || $chapter_slug ) {
			$cp->is_cluster = true;

			$doc_slug = $q->get( 'cp_doc' );

			// It's a single doc
			if ( $doc_slug ) {
				$doc = get_posts( array(
					'name'        => $doc_slug,
					'post_type'   => 'cp_doc',
					'numberposts' => 1,
				) );

				$doc = get_post( reset( $doc ) );

				if ( empty( $doc->ID ) ) {
					$q->set_404();
					return;
				}

				// Edit the content to add anchors to title elements.
				$doc->post_content = cp_doc_toc_content( $doc->post_content, $doc );

				$cp->cluster->displayed_object = $doc;
				$q->set( 'cp_post_title', $doc->post_title );

			// It's a doc preview
			} elseif ( isset( $_GET['doc_preview_id'] ) && isset( $_GET['doc_preview_nonce'] ) ) {
				$doc_id = (int) $_GET['doc_preview_id'];

				if ( false === wp_verify_nonce( $_GET['doc_preview_nonce'], 'cp_doc_preview_' . $doc_id ) ) {
					wp_die( __( 'Désolé vous n\'êtes pas autorisé à prévisualiser la documentation.', 'clusterpress-doc' ) );
				}

				$doc = get_post( $doc_id );

				if ( empty( $doc->ID ) ) {
					$q->set_404();
					return;
				} else {
					$doc->is_preview = true;
				}

				$cp->cluster->displayed_object = $doc;
				$q->set( 'cp_post_title', $doc->post_title );

			// It's a list of docs
			} else {

				// Check the chapter exists
				if ( $chapter_slug ) {
					$term = get_term_by( 'slug', $chapter_slug, 'cp_chapter' );

					if ( empty( $term ) ) {
						$q->set_404();
						return;
					}

					$cp->cluster->current_taxonomy = $term;
				}

				$cp->cluster->archive = cp_doc_get_post_type_archive_slug();
				$q->set( 'cp_post_title', cp_doc_get_post_type_archive_title() );
				$q->is_post_type_archive = false;
			}

			$cp->cluster->current = $this->id;
		}

		parent::parse_query( $q );
	}

	/**
	 * Set the document title for the displayed documentation page.
	 *
	 * @since  1.0.0
	 *
	 * @param array $title_parts The parts of the document title.
	 */
	public function set_subsite_page_title( $title_parts = array() ) {

		if ( cp_is_docs() ) {
			$title_parts['title'] = cp_doc_get_post_type_archive_title();

			$current_term = cp_current_taxonomy();

			if ( ! empty( $current_term->name ) ) {
				$title_parts['chapter'] = $current_term->name;
			}
		}

		parent::setup_page_title( $title_parts );
	}

	/**
	 * Return the Doc settings
	 *
	 * @since 1.0.0.
	 *
	 * @return  array The list of settings for the Cluster.
	 */
	public function get_settings() {
		if ( ! is_multisite() ) {
			return array();
		}

		$settings = array(
			'sections' => array(
				'doc_cluster_main_settings' => array(
					'title'    => __( 'Réglages principaux', 'clusterpress-doc' ),
					'callback' => 'cp_core_main_settings_callback',
				),
			),
			'fields' => array(
				'doc_cluster_main_settings' => array(
					'clusterpress_doc_headings' => array(
						'title'             => __( 'Balises de titre autorisées.', 'clusterpress-doc' ),
						'callback'          => 'cp_doc_headings_settings',
						'sanitize_callback' => 'cp_doc_sanitize_headings',
					),
					'clusterpress_doc_back_to_tops' => array(
						'title'             => __( 'Liens de retour en haut de page.', 'clusterpress-doc' ),
						'callback'          => 'cp_doc_back_to_tops_settings',
						'sanitize_callback' => 'intval',
					),
					'clusterpress_doc_chapter_checkboxes' => array(
						'title'             => __( 'Comportement des cases à cocher de chapitres', 'clusterpress-doc' ),
						'callback'          => 'cp_doc_chapter_checkboxes_settings',
						'sanitize_callback' => 'intval',
					),
				),
			),
		);

		if ( ! cp_cluster_is_enabled( 'site' ) ) {
			unset( $settings['fields']['doc_cluster_main_settings']['clusterpress_doc_chapter_checkboxes'] );
		}

		return $settings;
	}

	/**
	 * Return the specific feedbacks for the Doc Cluster.
	 *
	 * @since  1.0.0
	 *
	 * @return array The list of feedbacks specific to this Cluster.
	 */
	public function get_feedbacks() {
		return array(
			'doc-not-allowed' => array( 'cluster' => 'site', 'message' => __( 'Vous n\'êtes pas autorisé(e) à effectuer cette action sur la documentation de ce site.', 'clusterpress-doc' ) ),
			'doc-inserted'    => array( 'cluster' => 'site', 'message' => __( 'Ressource ajoutée avec succès.', 'clusterpress-doc' ) ),
			'doc-updated'     => array( 'cluster' => 'site', 'message' => __( 'Ressource mise à jour avec succès.', 'clusterpress-doc' ) ),
			'doc-published'   => array( 'cluster' => 'site', 'message' => __( 'Ressource publiée avec succès.', 'clusterpress-doc' ) ),
			'doc-error'       => array( 'cluster' => 'site', 'message' => __( 'Une erreur est survenue. L\'opération en cours a été interrompue.', 'clusterpress-doc' ) ),
			'doc-missing'     => array( 'cluster' => 'site', 'message' => __( 'Merci de vous assurer que le titre et le contenu de votre ressource ne soient pas vides.', 'clusterpress-doc' ) ),
			'doc-deleted'     => array( 'cluster' => 'site', 'message' => __( 'Ressource supprimée avec succès.', 'clusterpress-doc' ) ),
			'editing-doc'     => array( 'cluster' => 'site', 'message' => __( 'La ressource est actuellement en cours d\'édition. Vos modifications ne seront pas prises en compte.', 'clusterpress-doc' ) ),
			'editing-doc-alt' => array( 'cluster' => 'site', 'message' => __( 'La ressource est actuellement en cours d\'édition. Merci de patienter jusqu\'à ce qu\'elle se libère.', 'clusterpress-doc' ) ),
		);
	}

	/**
	 * Return the specific cap maps for the Doc Cluster.
	 *
	 * @since  1.0.0
	 *
	 * @return array The list cap maps specific to this Cluster.
	 */
	public function get_caps_map() {
		return array(
			'cp_doc_edit_documentation' => 'cp_doc_site_section_required_caps',
		);
	}

	/**
	 * Load Translations.
	 *
	 * @since 1.0.0
	 */
	public function load_languages() {
		$mofile = sprintf( $this->path_data['dir'] . 'languages/clusterpress-doc-%s.mo', get_locale() );
		cp_load_textdomain( 'clusterpress-doc', $mofile );
	}
}
