<?php
/**
 * Doc's Filters.
 *
 * @package ClusterPress Doc\doc
 * @subpackage filters
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Used to temporarly filter the ClusterPress template stack
 * to include this plugin's templates dir.
 *
 * @since  1.0.0
 *
 * @param  array  $stack The ClusterPress template stack.
 * @return array         The ClusterPress template stack.
 */
function cp_doc_set_template_stack_entries( $stack = array() ) {
	$index = max( array_keys( $stack ) ) + 1;

	$stack[ $index ] = trailingslashit( clusterpress()->doc->path_data['dir'] ) . 'templates';

	return $stack;
}

/**
 * Highlight the Doc wp_nav_menu if needed
 *
 * @since 1.0.0
 *
 * @param $menu_items The list of nav items.
 * @return            The list of nav items.
 */
function cp_doc_get_nav_menu_items( $menu_items = array() ) {
	if ( ! cp_is_docs() && ! cp_is_doc() ) {
		return $menu_items;
	}

	$doc_link = trim( get_post_type_archive_link( 'cp_doc' ), '/' );

	foreach ( $menu_items as $mk => $mv ) {
		if ( false !== strpos( $mv->url, $doc_link ) ) {
			$menu_items[ $mk ]->classes = array_merge( $mv->classes, array( 'current-menu-item', 'current_page_item' ) );
		}
	}

	return $menu_items;
}

/**
 * Override the Loop's global
 *
 * @since  1.0.0
 *
 * @param  CP_Cluster $cluster The current Cluster the loop is attached to.
 * @return CP_Cluster          The Doc Cluster.
 */
function cp_doc_site_posts_loop_global( $cluster = null ) {
	return clusterpress()->doc;
}

/**
 * Override the default Posts loop args.
 *
 * @since 1.0.0
 *
 * @param array $args {
 *   An array of arguments.
 *   @see WP_Query::parse_query() for a complete description.
 * }
 * @return array The overriden loop args for the documentation items.
 */
function cp_doc_has_posts_args( $args = array() ) {
	$args = array_merge( $args, array(
		'post_type' => 'cp_doc',
		'order'     => 'ASC',
		'orderby'   => 'title',
	) );

	// Depending on the displayed chapter, restrict results belonging to it.
	if ( cp_current_taxonomy() ) {
		$args['tax_query'] = array(
			array(
				'taxonomy' => 'cp_chapter',
				'field'    => 'term_id',
				'terms'    => (int) cp_current_taxonomy()->term_id,
		) );
	}

	return $args;
}

/**
 * Get the link to go back to the toc according to the context.
 *
 * @since  1.0.0
 *
 * @return string The link to the table of content.
 */
function cp_doc_get_toc_link( $html_text = '' ) {
	$url  = get_post_type_archive_link( 'cp_doc' );
	$text = __( 'Afficher la table des matières', 'clusterpress-doc' );

	if ( cp_current_taxonomy() ) {
		$url  = get_term_link( cp_current_taxonomy(), 'cp_chapter' );
		$text = sprintf( __( 'Afficher toutes les ressources du chapitre : %s', 'clusterpress-doc' ), cp_current_taxonomy()->name );
	}

	if ( ! $html_text ) {
		$html_text = $text;
	}

	/**
	 * Filter here to edit the link to the Table of content.
	 *
	 * @since  1.0.0
	 *
	 * @param  string $value The link to the Table of content.
	 */
	return apply_filters( 'cp_doc_get_toc_link', sprintf( '<a href="%1$s" title="%2$s">%3$s</a>',
		esc_url( $url ),
		esc_html( $text ),
		wp_kses( $html_text, array(
			'a'    => array( 'href' => true, 'class' => true, 'title' => true ),
			'span' => array( 'class' => true ),
		) )
	) );
}

/**
 * Customize the no results feedback for the documentation items.
 *
 * @since  1.0.0
 *
 * @param  string $message The default Site Posts loop feedback
 * @return string          A feedback customized for the documentation items.
 */
function cp_doc_get_no_docs_found( $message = '' ) {
	return sprintf( __( 'Aucune ressource n\'a été trouvée en réponse à votre recherche. %s', 'clusterpress-doc' ), cp_doc_get_toc_link() );
}

/**
 * Get the total count message for documentation items.
 *
 * @since  1.0.0
 *
 * @param  string  $total_count The total count message.
 * @param  integer $total       The amount of found documentation items
 * @param  integer $displayed   The number of documentation items displayed.
 * @return string               The total count message.
 */
function cp_doc_get_docs_total_count( $total_count = '', $total = 0, $displayed = 0 ) {
	$total_count  = sprintf(
		_n( '%1$s résultat affiché sur %2$s au total.', '%1$s résultat(s) affiché(s) sur %2$s au total.', $total, 'clusterpress-doc' ),
		$total,
		$displayed
	);

	return sprintf( '<span class="doc-toc-link">%1$s</span><span class="doc-pagination">%2$s</span>',
		cp_doc_get_toc_link( '<span class="dashicons dashicons-editor-ol"></span>' ),
		esc_html( $total_count )
	);
}

/**
 * Get the Doc's action Buttons for the search loop.
 *
 * @since 1.0.0
 *
 * @return string The Doc's action Buttons.
 */
function cp_doc_get_doc_actions( $actions = array() ) {
	$doc = cp_site_get_current_post();

	if ( empty( $doc->ID ) ) {
		return $actions;
	}

	$title = cp_site_get_title();

	$doc_actions = array(
		'view' => array(
			'wrapper_class' => 'view-post',
			'dashicon'      => 'dashicons-visibility',
			'content'       => sprintf(
				__( '<a href="%1$s" class="post-view-link">Consulter<span class="screen-reader-text"> "%2$s"</span></a>', 'clusterpress-doc' ),
				esc_url( get_permalink( $doc ) ),
				esc_html( $title )
			),
	) );

	if ( ! empty( $actions['edit'] ) ) {
		if ( current_user_can( 'edit_cp_docs' ) ) {
			$doc_actions['edit'] = $actions['edit'];
		}
	}

	if ( cp_cluster_is_enabled( 'site' ) && current_user_can( 'edit_cp_doc', array( 'site_id' => get_current_blog_id() ) ) ) {
		$site = cp_get_site_by( 'id', get_current_blog_id() );

		if ( ! empty( $site->blog_id ) ) {
			$doc_actions['edit'] = array(
				'wrapper_class' => 'edit-link',
				'dashicon'      => 'dashicons-edit',
				'content'       => sprintf(
					__( '<a href="%1$s" class="post-edit-link">Modifier<span class="screen-reader-text"> "%2$s"</span></a>', 'clusterpress-doc' ),
					esc_url( add_query_arg( cp_doc_get_site_action( 'edit' ), $doc->ID, cp_doc_get_site_url( $site ) ) ),
					esc_html( $title )
				),
			);
		}
	}

	/**
	 * Filter here to edit the Doc's actions
	 *
	 * @since 1.0.0
	 *
	 * @param array   $doc_actions The list of action buttons specific to the Doc.
	 * @param array   $actions     The initial list of action buttons.
	 * @param WP_Post $doc         The Doc object.
	 */
	return apply_filters( 'cp_doc_get_doc_actions', $doc_actions, $actions, $doc );
}

/**
 * Capability callback to check if current user can access the site's documentation.
 *
 * @since 1.0.0
 *
 * @param  int   $user_id The user ID.
 * @param  array $args    Adds the context to the cap. Typically the object ID.
 * @return array          The user's actual capabilities.
 */
function cp_doc_site_section_required_caps( $user_id = 0, $args = array() ) {
	// Defaults to the users archive caps.
	$caps = cp_user_get_default_caps();

	if ( ! cp_is_site() ) {
		return $caps;
	}

	if ( ! empty( $user_id ) && false !== array_search( cp_get_displayed_site_id(), cp_sites_get_user_sites( $user_id, 'any' ) ) ) {
		$caps = array( 'exist' );
	}

	/**
	 * Filter here to edit the site's documentation caps
	 *
	 * @since 1.0.0
	 *
	 * @param  array $caps    The user's actual capabilities.
	 * @param  int   $user_id The user ID.
	 * @param  array $args    Adds the context to the cap. Typically the object ID.
	 */
	return apply_filters( 'cp_doc_site_section_required_caps', $caps, $user_id, $args );
}

/**
 * Map Doc caps for the displayed site.
 *
 * @since  1.0.0
 *
 * @param  array  $caps    List of Caps for the current capability check.
 * @param  string $cap     The current capability check.
 * @param  int    $user_id The current user ID.
 * @param  array  $args    Additional arguments for the capability check.
 * @return array           List of Caps for the current capability check.
 */
function cp_doc_map_caps( $caps, $cap, $user_id, $args ) {
	if ( ! cp_is_site_doc() && empty( $args[0]['site_id'] ) ) {
		return $caps;
	}

	$is_site_admin = is_super_admin();
	if ( ! $is_site_admin && cp_displayed_site() && false !== array_search( $user_id, cp_displayed_site()->admins ) ) {
		$is_site_admin = true;
	}

	switch ( $cap ) {
		case 'publish_cp_docs'        :
		case 'edit_cp_doc'            :
		case 'assign_cp_doc_chapters' :
			$site_id = cp_get_displayed_site_id();

			if ( ! empty( $args[0]['site_id'] ) ) {
				$site_id = (int) $args[0]['site_id'];
			}

			$user_sites = cp_sites_get_user_sites( $user_id, 'any' );

			if ( $is_site_admin || ( ! empty( $user_sites ) && false !== array_search( $site_id, $user_sites ) ) ) {
				$caps = array( 'exist' );
			}

			break;

		case 'delete_cp_doc' :
			if ( $is_site_admin ) {
				$caps = array( 'exist' );

			// Other users of the site
			} elseif ( ! empty( $args[0] ) && is_a( $args[0], 'WP_Post' ) ) {
				$doc = get_post( $args[0] );

				// Only authors can trash their own content.
				if ( (int) $user_id === (int) $doc->post_author ) {
					$caps = array( 'exist' );
				}
			}

			break;

		case 'edit_cp_docs'             :
		case 'edit_others_cp_docs'      :
		case 'publish_cp_docs'          :
		case 'read_private_cp_docs'     :
		case 'delete_cp_docs'           :
		case 'delete_private_cp_docs'   :
		case 'delete_published_cp_docs' :
		case 'delete_others_cp_docs'    :
		case 'edit_private_cp_docs'     :
		case 'edit_published_cp_docs'   :
		case 'manage_cp_doc_chapters'   :
		case 'edit_cp_doc_chapters'     :
		case 'delete_cp_doc_chapters'   :
			$cap = array( 'edit_others_posts' );

			break;
	}

	return apply_filters( 'cp_doc_map_caps', $caps, $cap, $user_id, $args );
}
add_filter( 'cp_map_meta_caps', 'cp_doc_map_caps', 10, 4 );

/**
 * Customize the WP Editor buttons for the doc editor.
 *
 * @since  1.0.0
 *
 * @param  array  $buttons   The list of active WP Editor buttons.
 * @param  string $editor_id The doc editor CSS id.
 * @return array             The list of active doc editor buttons.
 */
function cp_doc_editor_button_filter( $buttons = array(), $editor_id = '' ) {
	if ( 'cp-doc-content' !== $editor_id ) {
		return $buttons;
	}

	$remove_buttons = array(
		'wp_more',
		'spellchecker',
		'wp_adv',
		'fullscreen',
	);

	// Remove unused buttons
	$buttons = array_diff( $buttons, $remove_buttons );

	// Add specific buttons
	array_push( $buttons, 'image', 'fullscreen' );

	return $buttons;
}

/**
 * Customize the WP Editor tinyMCE settings for the doc editor.
 *
 * @since  1.0.0
 *
 * @param  array  $tiny      The list of tinyMCE settings.
 * @param  string $editor_id The doc editor CSS id.
 * @return array             The list of tinyMCE settings.
 */
function cp_doc_editor_before_init( $tiny = array(), $editor_id = '' ) {
	if ( 'cp-doc-content' !== $editor_id ) {
		return $tiny;
	}

	/**
	 * Set block formats according to settings.
	 */
	$headings        = cp_doc_get_available_editor_headings();
	$active_headings = array_intersect_key( $headings, array_flip( cp_doc_get_headings() ) );

	if ( ! array_diff_key( $headings, $active_headings ) ) {
		return $tiny;
	}

	$editor_headings = '';
	foreach ( $active_headings as $kh => $vh ) {
		$editor_headings .= $vh . '=' . $kh . ';';
	}

	$tiny['block_formats'] = 'Paragraph=p;' . $editor_headings . 'Preformatted=pre';

	return $tiny;
}

/**
 * Outputs filter options into the documentation section of the site's discovery page.
 *
 * @since 1.0.0
 *
 * @param  array $options The ClusterPress filter options
 * @return array          The ClusterPress doc available post stati.
 */
function cp_doc_status_set_options( $options = array() ) {
	if ( ! cp_is_site_doc() ) {
		return $options;
	}

	$options = array( array(
		'value' => 'all',
		'text'  => __( 'Tout les états', 'clusterpress-doc' )
	) );

	foreach ( cp_doc_get_editable_stati( 'edit' ) as $value => $text ) {
		$options[] = array( 'value' => $value, 'text' => $text );
	}

	return $options;
}
add_filter( 'cp_doc_status_get_options', 'cp_doc_status_set_options', 10, 1 );

/**
 * Customize paginate args for the Docs loop.
 *
 * @since 1.0.0
 *
 * @param  array  $paginate_args The pagination args to use for the paginate_links() function
 * @return array                 The pagination args
 */
function cp_doc_loop_paginate_args( $paginate_args = array() ) {
	// We only need to edit the pretty links.
	if ( ! clusterpress()->permalink_structure ) {
		return $paginate_args;
	}

	// Documentation section of the site's discovery page.
	if ( cp_is_site_doc() ) {
		$paginate_args['base'] = trailingslashit( cp_site_get_url( array(
			'rewrite_id'    => 'cp_site_doc',
			'slug'          => cp_doc_get_post_type_archive_slug(),
		), cp_displayed_site() ) ) . '%_%';
	}

	if ( cp_is_docs() ) {
		$base = get_post_type_archive_link( 'cp_doc' );

		if ( cp_current_taxonomy() ) {
			$base = get_term_link( cp_current_taxonomy(), 'cp_chapter' );
		}

		$paginate_args['base'] = $base . '%_%';
	}

	return $paginate_args;
}
add_filter( 'cp_sites_posts_loop_paginate_args', 'cp_doc_loop_paginate_args', 10, 1 );

/**
 * Edit the Previous adjacent sort order.
 *
 * @since 1.0.0
 *
 * @param  string The initial order SQL clause.
 * @return string The new ordrer SQL clause.
 */
function cp_doc_adjacent_sort_previous( $order = '' ) {
	return 'ORDER BY p.post_title DESC LIMIT 1';
}

/**
 * Edit the Next adjacent sort order.
 *
 * @since 1.0.0
 *
 * @param  string The initial order SQL clause.
 * @return string The new ordrer SQL clause.
 */
function cp_doc_adjacent_sort_next( $order = '' ) {
	return 'ORDER BY p.post_title ASC LIMIT 1';
}

function cp_doc_scripts_data( $scripts_data = array() ) {
	if ( ! cp_is_site_doc() ) {
		return $scripts_data;
	}

	$scripts_data['strings']['cpDocConfirm'] = __( 'Etes-vous certains de vouloir supprimer cette ressource ?', 'clusterpress-doc' );
	$scripts_data['docSettings'] = array(
		'cb'    => (int) cp_doc_is_multi_chapters_enabled(),
		'pulse' => '30',
	);

	return $scripts_data;
}
add_filter( 'cp_scripts_data', 'cp_doc_scripts_data', 10, 1 );

/**
 * Listen to heartbeat to eventually lock the doc on front end.
 *
 * @since  1.0.0
 *
 * @param  array  $response The heartbeat response.
 * @param  array  $data     The data sent.
 * @return array            The heartbeat response.
 */
function cp_doc_is_locked( $response = array(), $data = array() ) {
	if ( empty( $data['cp_doc_id'] ) || empty( $data['cp_site_id'] ) ) {
		return $response;
	}

	$user_id     = cp_doc_lock_post( $data['cp_doc_id'], $data['cp_site_id'] );
	$cp_response = array( 'cp_doc_lock' => $user_id );

	if ( ! empty( $user_id ) ) {
		$cp_response['avatar'] = sprintf( '<div class="locked"><span class="dashicons dashicons-lock"></span> %s</div>',
			cp_get_user_avatar( array(
				'user_id' => $user_id,
				'width'   => 18,
				'height'  => 18,
			) )
		);
	}

	return array_merge( $response, $cp_response );
}
add_filter( 'heartbeat_received', 'cp_doc_is_locked', 10, 2 );

/**
 * Makes sure Embed codes are interpreted.
 *
 * @since 1.0.0
 *
 * @param  string $content The doc content.
 * @return string          The doc content.
 */
function cp_doc_autoembed( $content = '' ) {
	global $wp_embed;

	return $wp_embed->autoembed( $content );
}

/** Template Tags Output Filters **********************************************/

add_filter( 'cp_doc_get_content', 'wptexturize',                       10 );
add_filter( 'cp_doc_get_content', 'convert_smilies',                   20 );
add_filter( 'cp_doc_get_content', 'wpautop',                           10 );
add_filter( 'cp_doc_get_content', 'shortcode_unautop',                 10 );
add_filter( 'cp_doc_get_content', 'prepend_attachment',                10 );
add_filter( 'cp_doc_get_content', 'wp_make_content_images_responsive', 10 );
add_filter( 'cp_doc_get_content', 'do_shortcode',                      11 );
add_filter( 'cp_doc_get_content', 'cp_doc_autoembed',                  12 );

add_filter( 'cp_doc_to_edit_get_content', 'wp_unslash',         5 );
add_filter( 'cp_doc_to_edit_get_content', 'wpautop'               );
add_filter( 'cp_doc_to_edit_get_content', 'shortcode_unautop', 10 );
